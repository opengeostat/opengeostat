==================================================================
opengeostatdb: Python package for Mineral Resource Database
==================================================================

Opengeostat Database is the module to organize and manipulate data



Installation
------------

The easiest way to install most Python packages is via ``easy_install`` or ``pip``::

    $ easy_install opengeostatdb

Usage
-----

TODO: This is a good place to start with a couple of concrete examples of how the package should be used.

The boilerplate code provides a dummy ``main`` function that prints out the word 'Hello'::

    >> from opengeostatdb import main

    
When the package is installed via ``easy_install`` or ``pip`` this function will be bound to the ``opengeostatdb`` executable in the Python installation's ``bin`` directory (on Windows - the ``Scripts`` directory).
