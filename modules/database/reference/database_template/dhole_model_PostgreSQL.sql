/*
 ___________________________________________________________________

 DRILLHOLE SQL DEFINITION  

 Created:     Adrian Martinez Vargas, April 2015
              http://www.opengeostat.com/

 License:     GNU General Public License V3 
              http://www.gnu.org/licenses/
 
 Description: This is a database definition file optimized for 
              PostgreSQL
 ___________________________________________________________________
*/


/* ___________________________________________________________________
    
    Put here the parent tables 
   ___________________________________________________________________
*/

-- Remove tables if exist
/*
DROP TABLE IF EXISTS Assay;
DROP TABLE IF EXISTS Litho;
DROP TABLE IF EXISTS Lithologies;
DROP TABLE IF EXISTS Lab_Assay;
DROP TABLE IF EXISTS Survey;
DROP TABLE IF EXISTS Collar;
*/

-- Collar Table
CREATE TABLE Collar (
  BHID VARCHAR(45)  NOT NULL,
  X FLOAT  NOT NULL,
  Y FLOAT  NOT NULL,
  Z FLOAT  NOT NULL,
  EOH FLOAT  NOT NULL,
  Comments VARCHAR(250),
PRIMARY KEY(BHID));

-- List of valid lithologies
CREATE TABLE Lithologies (
  Lithology_ID VARCHAR(20)  NOT NULL,
  Description VARCHAR(50),
  Comments VARCHAR(250),
PRIMARY KEY(Lithology_ID));

-- assay as provided by the lab
DROP TABLE IF EXISTS Lab_Assay;
CREATE TABLE Lab_Assay (
  Samp_ID VARCHAR(50)  NOT NULL,
  Au FLOAT,
PRIMARY KEY(Samp_ID));

/* ___________________________________________________________________
    
    Put here the child tables (tables with foreign key)
   ___________________________________________________________________
*/

-- Define the Survey Table
CREATE TABLE Survey (
  BHID VARCHAR(45)  NOT NULL,
  AT FLOAT  NOT NULL,
  AZ FLOAT  NOT NULL,
  DIP FLOAT  NOT NULL,
  PRIMARY KEY(BHID, AT),          -- no duplicates (BHID, AT) allowed
  FOREIGN KEY(BHID)               -- only BHID in the collar table are allowed
    REFERENCES Collar(BHID)       
      ON DELETE CASCADE           -- This will remove/rename BHID if BHID is deleted 
      ON UPDATE CASCADE);         -- or changed in Collar table


-- define Lithology table
CREATE TABLE Litho (
  BHID VARCHAR(45)  NOT NULL,
  "From" FLOAT  NOT NULL,
  "To" FLOAT  NOT NULL,
  Lithology_ID VARCHAR(20)  NOT NULL,
  Log_memo VARCHAR(255),
  Comments VARCHAR(255),
 CONSTRAINT chk_interv 
    CHECK ("To">"From"),          -- ensure valid From To intervals
 CONSTRAINT chk_litho
 PRIMARY KEY(BHID, "From"),       -- no duplicates (BHID, FROM) allowed
 FOREIGN KEY(BHID) 
  REFERENCES Collar(BHID)         -- Remove or rename BHID if it changes at Collar Table 
   ON DELETE CASCADE 
   ON UPDATE CASCADE,
 FOREIGN KEY(Lithology_ID)         -- This will remove/rename LITHOLOGYID if it 
  REFERENCES                       -- changes at Lithocode Table                  
     Lithologies(Lithology_ID) 
   ON DELETE CASCADE 
   ON UPDATE CASCADE);

-- define assay table
CREATE TABLE Assay (
  BHID VARCHAR(45)  NOT NULL,
  "From" FLOAT  NOT NULL,
  "To" FLOAT  NOT NULL,
  Samp_ID VARCHAR(50)  NOT NULL,
  Comments VARCHAR(255),
CONSTRAINT chk_interv 
  CHECK ("To">"From"),             -- not allow zero thickness intervals or "To"""<"From" 
PRIMARY KEY(BHID, "From"),         -- no duplicates (BHID, FROM) allowed
  FOREIGN KEY(BHID)
    REFERENCES Collar(BHID)        
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(Samp_ID)
    REFERENCES Lab_Assay(Samp_ID)
      ON DELETE CASCADE
      ON UPDATE CASCADE);

/* ___________________________________________________________________
    
    Testing Area
    
    This is for test only 
   ___________________________________________________________________
*/


-- Testing collar
    -- this may work
    INSERT INTO Collar (BHID, X, Y, Z, EOH)  
    VALUES ('D1', 10, 10, 20, 15),
           ('D2', 20, 20, 20, 15),
           ('D3', 10, 30, 20, 15);
    
    -- this may rise an error
    INSERT INTO Collar (BHID, X, Y, Z, EOH)  
    VALUES ('D1', 10, 10, 20, 15);

-- add some data in lab assay and lithologies
    -- this may work
    INSERT INTO Lab_Assay 
    VALUES ('sample1', 15.5),
           ('sample2', 5.5),
           ('sample3', 0.05);

    INSERT INTO Lithologies (Lithology_ID, Description)
    VALUES ('G', 'Granite'),
           ('B', 'Basalt'),
           ('OB', 'Oberburden');
     
                          
    -- this may fail
    INSERT INTO Lab_Assay 
    VALUES ('sample1', 15.5);
    
    INSERT INTO Lithologies (Lithology_ID, Description)
    VALUES ('G', 'Granite');
   
    -- this may work    
    INSERT INTO Survey (BHID, AT, AZ, DIP)  
    VALUES ('D1', 0, 10, 20),
           ('D1', 10, 10, 20),
           ('D2', 0, 20, 20),
           ('D2', 20, 20, 20),
           ('D3', 0, 30, 20),
           ('D3', 10, 30, 20);

    -- this may fail (Unique constraint)
    INSERT INTO Survey (BHID, AT, AZ, DIP)  
    VALUES ('D1', 0, 10, 20);
    
    -- this may fail (YYYY is not in Collar.BHID)
    INSERT INTO Survey (BHID, AT, AZ, DIP)  
    VALUES ('YYYYYYY', 0, 10, 20);

    
    -- this may work        
    INSERT INTO litho (BHID, "From", "To", Lithology_ID)  
    VALUES ('D1', 0, 10, 'OB'),
           ('D1', 10, 20, 'B'),
           ('D1', 20, 30, 'G');
           
    -- this may fail, ('D1', 0) duplicated      
    INSERT INTO Litho (BHID, "From", "To", Lithology_ID)  
    VALUES ('D1', 0, 10, 'OB');

    -- this may fail, YYYY not in Collar    
    INSERT INTO Litho (BHID, "From", "To", Lithology_ID)  
    VALUES ('YYYY', 0, 10, 'OB');
    
    -- this may fail, no anorthosite in Lithologies 
    INSERT INTO Litho (BHID, "From", "To", Lithology_ID)  
    VALUES ('D1', 50, 100, 'anorthosite');

    -- this may fail, but it didn't 
    -- not detectic overlap... Check Not Implemented !!!!!!!!
    -- interval From 5 To 9 overlaping interval From 0 to 10 
    INSERT INTO Litho (BHID, "From", "To", Lithology_ID)  
    VALUES ('D1', 5, 9, 'B');

          
    -- this may work
    INSERT INTO assay (BHID, "From", "To", Samp_ID)  
    VALUES ('D1', 0, 10, 'sample1'),
           ('D1', 10, 20, 'sample2');

    -- may fail, but it didn't 
    -- Sample ID duplicate check not Implemented
    -- Possible implementation problem, Duplication required 
    -- for non-sampled intervals
    -- TODO implement check, Use TRIGGERS to check 
    -- make sure to accept duplicates only in non-sampled intervals 
    INSERT INTO assay (BHID, "From", "To", Samp_ID)  
    VALUES ('D1', 20, 30, 'sample1');
