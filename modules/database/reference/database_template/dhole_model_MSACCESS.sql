/*
 ___________________________________________________________________

 DRILLHOLE SQL DEFINITION For Microsoft Access

 Created:     Adrian Martinez Vargas, April 2015
              http://www.opengeostat.com/

 License:     GNU General Public License V3 
              http://www.gnu.org/licenses/
 
 Description: This is a database definition file optimized for 
              Microsoft Access.

              Microsoft Access do not support multiple lines,
              you may copy and execute each statement per separate 
	      in a SQL view. 
 ___________________________________________________________________
*/

-- create tables in the same order

-- Collar Table
CREATE TABLE Collar (
  BHID VARCHAR(45)  NOT NULL,
  X FLOAT  NOT NULL,
  Y FLOAT  NOT NULL,
  Z FLOAT  NOT NULL,
  EOH FLOAT  NOT NULL,
  Comments VARCHAR(250),
PRIMARY KEY(BHID));

-- List of valid lithologies
CREATE TABLE Lithologies (
  Lithology_ID VARCHAR(20)  NOT NULL,
  Description VARCHAR(50),
  Comments VARCHAR(250),
PRIMARY KEY(Lithology_ID));

--assay as provided by the lab
CREATE TABLE Lab_Assay (
  Samp_ID VARCHAR(50)  NOT NULL,
  Au FLOAT,
PRIMARY KEY(Samp_ID));


-- Define the Survey Table
CREATE TABLE Survey (
  BHID VARCHAR(45)  NOT NULL,
  AT FLOAT  NOT NULL,
  AZ FLOAT  NOT NULL,
  DIP FLOAT  NOT NULL,
  PRIMARY KEY(BHID, AT),          
  FOREIGN KEY(BHID)            
    REFERENCES Collar(BHID));     

-- ON DELETE CASCADE and ON UPDATE CASCADE not supported
-- add this manually on Database tools/relationships



-- define Lithology table
CREATE TABLE Litho (
  BHID VARCHAR(45)  NOT NULL,
  [From] FLOAT  NOT NULL,
  [To] FLOAT  NOT NULL,
  Lithology_ID VARCHAR(20)  NOT NULL,
  Log_memo VARCHAR(255),
  Comments VARCHAR(255),       
 CONSTRAINT chk_litho
 PRIMARY KEY(BHID, [From]),     
 FOREIGN KEY(BHID) 
  REFERENCES Collar(BHID),
 FOREIGN KEY(Lithology_ID)     
  REFERENCES                         
     Lithologies(Lithology_ID));

-- CONSTRAINT CHECK ([To]>[From]) not supported use validation 
-- (Property Sheet) rule to update
-- ON DELETE CASCADE and ON UPDATE CASCADE not supported
-- add this manually on Database tools/relationship

-- define Assay table
CREATE TABLE Assay (
  BHID VARCHAR(45)  NOT NULL,
  [From] FLOAT  NOT NULL,
  [To] FLOAT  NOT NULL,
  Samp_ID VARCHAR(50)  NOT NULL,
  Comments VARCHAR(255),           
PRIMARY KEY(BHID, [From]),         
  FOREIGN KEY(BHID)
    REFERENCES Collar(BHID),
  FOREIGN KEY(Samp_ID)
    REFERENCES Lab_Assay(Samp_ID));
      
-- CONSTRAINT CHECK ([To]>[From]) not supported use validation 
-- (Property Sheet) rule to update
-- ON DELETE CASCADE and ON UPDATE CASCADE not supported
-- add this manualy on Database tools/relationsheps
