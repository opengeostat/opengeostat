# OpenGeostat


OpenGeostat is a python module for geostatistics with focus on the mining industry. The aim of the project is to provide tools for the entire workflow in a classical resource estimation: 

* database and geological processing
    * drillhole database and drillhole management, table merging, compositing and desurveying (coordinates calculation) 
    * geological modeling, coding, sample selection withing solid or region
    * block modeling, block codding

* geostatistics and statistics
    * statistical analysis 
    * variography
    * histogram modeling and change of support
    * interpolation and simulation
    * tools to validate estimations

The database and geological processing will be developed as an independent module and as a [FreeCad](http://forehead/) extension. The geostatistics and statistics part will be an independent python module. 


# Lisence
The Opengeostat code will be released under  MIT license. 

Opengeostat Documentation will be released under [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/) ![CC license](https://i.creativecommons.org/l/by/4.0/80x15.png)


# How to contribute
There are various ways 

Programming new tools, add functionality, fix bugs. For this clone the project using the code: 

```
$ git clone https://opengeostat@bitbucket.org/opengeostat/opengeostat.git
```

Request new tools directly to info(at)opengeostat.com. 

Support the project with on-demand code. This will have a cost associated.

